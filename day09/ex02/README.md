Datastore is an automatically scaling NoSQL Database as a Service (DBaaS) on the Google Cloud Platform whilst MySQL is a 
relational database MySQL is a relational database management system based on SQL – Structured Query Language. The application 
is used for a wide range of purposes, including data warehousing, e-commerce, and logging applications. Datastore is a NoSQL 
document database built for automatic scaling, high performance, and ease of application development. Datastore features include: 
Atomic transactions. Datastore can execute a set of operations where either all succeed, or none occur. High availability of reads 
and writes.
