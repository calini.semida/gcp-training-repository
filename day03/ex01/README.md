Compute Engine instance is a virtual machine (VM) hosted on Google's infrastructure.
It allows clients to run workloads on Google's physical hardware.

