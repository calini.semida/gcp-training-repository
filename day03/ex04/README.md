In the IPv4 IP address space, there are five classes: A, B, C, D and E.
Each class has a specific range of IP addresses (and ultimately dictates the number of devices you can have on your network).
Primarily, class A, B, and C are used by the majority of devices on the Internet. Class D and class E are for special uses.
Class A for networks with large number of total hosts.
Class B addresses are for medium to large sized networks.
Class C addresses are used in small local area networks (LANs).
Class D IP addresses are not allocated to hosts and are used for multicasting.
Class E IP addresses are not allocated to hosts and are not available for general use. These are reserved for research purposes.
