External IP addresses must be publicly routable IP addresses. Resources with external IP addresses can communicate with the public
internet.Internal IP addresses are not publicly advertised. They are used only within a network.
