A Virtual Machine (VM) is a compute resource that uses software instead of a physical computer to run
programs and deploy apps. Each virtual machine runs its own operating system and functions separately 
from the other VMs, even when they are all running on the same host.
