A soft link is an actual link to the original file, whereas a hard link is a mirror copy of the original file. 
If you delete the original file, the soft link has no value, because it has only the path of the original file, not the contents.
But in the case of hard link, even if you delete the original file, the hard link has the actual content of the original file.
Soft link has different inode number and file permissions than original file.
Hard link has the same inode number and file permissions of the original file.



