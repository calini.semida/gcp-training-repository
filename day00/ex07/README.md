HEAD method is same as GET but only transfers the status line and header section.
It is used to ask only for information about a document, not for the document itself.
X-Powered-By header is a common non-standard HTTP response header.
It's often included by default in responses constructed via a particular scripting technology.
