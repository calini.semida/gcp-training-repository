Every Linux file is assigned 3 type of owners: user, group, others and has 3 access permissions: Read, Write and Execute.
For eg: drwxr-xr-x -> d indicates directory, rwx indicates read, write, execute permissions for file owner, r-x indicates read, execute permissions for 
group owner and r-x read, execute permissions for others. To change that, use CHMOD command line. chmod u=rwx filename or chmod [3digitnumber] filename 
where number means. 0 = none 1= --x, 2= -w-, 3= -wx, 4= r--, 5= r-x, 6=rw-, 7=rwx.    
 

