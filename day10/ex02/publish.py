from google.cloud import pubsub_v1

project_id = "pacific-vault-327620"
topic_id = "Fully-automated"

publisher = pubsub_v1.PublisherClient()
topic_path = publisher.topic_path(project_id, topic_id)

for n in range(1, 2):
    data = "This message tests if pushing works in exercise 02"
    data = data.encode("utf-8")
    future = publisher.publish(topic_path, data)
    print(future.result())

print(f"Published messages to {topic_path}.")