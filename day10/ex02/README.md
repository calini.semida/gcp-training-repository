While synchronous messaging is a live person-to-person conversation, asynchronous communication doesn’t require both parties 
to be present and speaking at the same time. This is great for the customer because they are able to start, pause, and resume a 
conversation around their life. Additionally, asynchronous messaging provides a better experience than synchronous messaging for 
complex issues that require more than one sitting or agent to fix.
