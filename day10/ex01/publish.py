from google.cloud import pubsub_v1


project_id = "pacific-vault-327620"
topic_id = "starting-with-UI"

publisher = pubsub_v1.PublisherClient()
topic_path = publisher.topic_path(project_id, topic_id)

for n in range(1, 6):
    data = f"This is sent from code message id: {n}"
    data = data.encode("utf-8")
    future = publisher.publish(topic_path, data)
    print(future.result())

print(f"Published messages to {topic_path}.")