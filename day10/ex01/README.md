Message-oriented middleware (MOM) is software or hardware infrastructure supporting sending and receiving messages between 
distributed systems. The middleware creates a distributed communications layer that insulates the application developer from 
the details of the various operating systems and network interfaces.
