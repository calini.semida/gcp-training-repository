OLTP and OLAP both are the online processing systems. OLTP is a transactional processing while OLAP is an analytical processing system.
OLTP is a system that manages transaction-oriented applications on the internet for example, ATM. OLAP is an online system that reports
to multidimensional analytical queries like financial reporting, forecasting, etc.

The basic difference between OLTP and OLAP is that OLTP is an online database modifying system, whereas, OLAP is an online database query answering system.
OLTP is an Online Transaction Processing system. The main focus of OLTP system is to record the current Update, Insertion and Deletion while transaction.
The OLTP queries are simpler and short and hence require less time in processing, and also requires less space.
OLAP is an Online Analytical Processing system. OLAP database stores historical data that has been inputted by OLTP.
The transaction in OLAP are long and hence take comparatively more time for processing and requires large space.
The transactions in OLAP are less frequent as compared to OLTP.The example for OLAP is to view a financial report,
or budgeting, marketing management, sales report, etc.
