from flask import Flask, render_template, request, json
import requests
import os

t_dir = os.path.abspath("app/templates")
s_dir = os.path.abspath("app/static")

app = Flask(__name__, template_folder=t_dir, static_folder=s_dir)

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/', methods=['POST', 'GET'])
def post():
    country = request.form['country']
    country = str(country)
    get_request = requests.get('https://covid-api.mmediagroup.fr/v1/vaccines')
    data = json.loads(get_request.content)
    return render_template('index.html', data=data, country=country), 201
if __name__ == '__main__':
    app.run()
