Google BigQuery is a cloud-based big data analytics web service for processing very large read-only data sets. BigQuery was designed for analyzing data on 
the order of billions of rows, using a SQL-like syntax. BigQuery is a business intelligence/OLAP (online analytical processing) system, it uses SQL dialects 
and is based on Google's internal column-based data processing technology.
