The main difference is that databases are organized collections of stored data. Data warehouses are information systems built from 
multiple data sources - they are used to analyze data. Databases are most useful for the small, atomic transactions. Data warehouses are 
best suited for larger questions that require a higher level of analysis.Databases can handle thousands of users at one time. Data 
warehouses can only handle a smaller number. Databases are most useful for the small, atomic transactions. Data warehouses are best suited 
for larger questions that require a higher level of analysis.
