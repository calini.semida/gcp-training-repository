Datastore is designed to support transactional workloads, such as the backend for a web application.
It's optimized for small transactions that read or write a limited number of rows per operation, with strong consistency guarantees.

BigQuery is designed for analytic workloads. It's append-only, and it's optimized for queries that scan/filter/aggregate entire tables of data to get 
answers out of your data.
