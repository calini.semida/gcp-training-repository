def raise_to_three(src_list):
    return [x**3 for x in src_list]
print(raise_to_three([2, 3, 4, 5]))