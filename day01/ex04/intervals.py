def interval():
    while True:
        try:
            a,b = map(int, input().split())
        except TypeError:
            print("Incorrect type")
            break
        except EOFError:
            print("No input provided")
            break
        except ValueError:
            print("Invalid interval")
            break
        else:
                if a <= b:
                    for x in range(a, b+1):
                      print(x, end=" ") 
                    print("\n")
                    break
def main():
    interval()
    
if __name__== "__main__" :
    main()