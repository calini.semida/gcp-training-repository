Two types of Error occurs in python: syntax errors and exceptions. Error caused by not following the proper structure (syntax) of the 
language is called syntax error or parsing error. For example, when you forget : after and if statement or ) or an indent.
Errors that occur at runtime (after passing the syntax test) are called exceptions or logical errors. For instance, they occur when we try 
to open a file(for reading) that does not exist (FileNotFoundError), try to divide a number by zero (ZeroDivisionError), or try to 
import a module that does not exist (ImportError).
