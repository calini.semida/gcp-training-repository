A URL consists of five parts: the scheme, subdomain, top-level domain, second-level domain, and subdirectory.
The scheme tells web servers which protocol to use when it accesses a page on your website. Eg. HTTPS or HTTP.
A subdomain in a URL indicates which particular page of your website the web browser should serve up. Eg. mail.yahoo.com
Second-level domain (SLD) is the name of your website. Eg. www.yahoo.com
The top-level domain (TLD) specifies what type of entity your organization registers as on the internet.Eg. .com .ro
A subdirectory also known as a subfolder helps people understand which particular section of a webpage they're on. Eg. zara.com/hats

pip is a standard package manager used to install and maintain packages for Python. The Python standard library comes with
a collection of built-in functions and built-in packages.
