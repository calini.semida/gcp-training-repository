from flask import Flask, render_template
import os

t_dir = os.path.abspath('app/templates')
s_dir = os.path.abspath('app/static')

app = Flask(__name__, template_folder=t_dir, static_folder=s_dir)

@app.route('/hello_to_training/<name>/')

def hello_to_training(name):
  return render_template('public/indexname.html', name=name)

if __name__ == "__main__":
    app.run(port=8080)