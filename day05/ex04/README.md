The primary or most-commonly-used HTTP methods are POST, GET, PUT, and DELETE. These correspond to create, read, update, and delete (or CRUD) operations, respectively. There are a number of other methods, too, but are utilized less frequently, one of which is PATCH.
POST - CREATE Used to create new resources, NOT SAFE - NOT IDEMPOTENT. Making two identical POST will duplicate the resource.
GET - READ Retrieve a representation of a resource, SAFE - IDEMPOTENT. Multiple identical requests return the same result.
PUT - UPDATE Update or Create a resource, NOT SAFE - IDEMPOTENT. Multiple identical requests will update the same resource.
PATCH request on the other hand, is used to make changes to part of the resource at a location. That is, it PATCHES the resource.
DELETE - DELETE Used to delete a resource, NOT SAFE - IDEMPOTENT. Multiple identical requests will delete same resource.

JavaScript Object Notation (JSON) is a standard text-based format for representing structured data based on JavaScript object syntax.
It is commonly used for transmitting data in web applications (e.g., sending some data from the server to the client, so it can be
displayed on a web page, or vice versa).

CRUD stands for Create, Read, Update, and Delete. But put more simply, in regards to its use in RESTful APIs, CRUD is the standardized
 use of HTTP methods.
