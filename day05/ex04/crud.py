from flask import Flask, request, jsonify, json
from google.cloud import datastore

kind = "user-db"
datastore_client = datastore.Client()

app = Flask(__name__)

@app.route('/')
def index():
    return 'WORKING'
    
@app.route('/user/add', methods=['POST'])
def add_user():
    myid = request.json['id']
    myemail = request.json['email']
    myname = request.json['name']
    
    myuser_key = datastore_client.key(kind)
    myuser = datastore.Entity(key=myuser_key)

    myuser["id"] = myid
    myuser["email"] = myemail
    myuser["name"] = myname

    datastore_client.put(myuser)
    return "User added ", 201

@app.route('/user/list', methods=['GET']) 
def get_list():
    myuserlist = []
    myuserquery = datastore_client.query(kind=kind)
    myusers = myuserquery.fetch()
    for x in myusers: myuserlist.append(x)
    return jsonify(myuserlist)

@app.route('/user/<int:iduser>', methods=['GET'])
def getid(iduser):
    myuser_key = datastore_client.key(kind, iduser)
    myuser = datastore_client.get(myuser_key)
    return jsonify(myuser)

@app.route('/user/<int:iduser>', methods=['PUT'])
def update_user(iduser):
    update_name = request.json['name']
    update_email = request.json['email']

    update_user_key = datastore_client.key(kind, iduser)
    update_user = datastore_client.get(update_user_key)

    update_user['name'] = update_name
    update_user['email'] = update_email

    datastore_client.put(update_user)
    return "User updated ", 200

@app.route('/user/<int:iduser>', methods=['PATCH'])
def modify_email(iduser):
    modify_email = request.json['email']

    modify_user_key = datastore_client.key(kind, iduser)
    modify_user = datastore_client.get(modify_user_key)

    modify_user['email'] = modify_email
    datastore_client.put(modify_user)
    return "Email modified ", 200

@app.route('/user/<int:iduser>', methods=['DELETE'])
def delete_user(iduser):

    delete_user_key = datastore_client.key(kind, iduser)
    delete_user = datastore_client.get(delete_user_key)
    
    datastore_client.delete(delete_user)
    return "User deleted ", 200    