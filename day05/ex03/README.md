Google Cloud Datastore is a highly scalable, fully managed NoSQL database service offered by Google on the Google Cloud Platform.
Cloud storage is something that "allows you to save data and files in an off-site location that you access either through the public
internet or a dedicated private network connection."

REST API (also known as RESTful API) is an application programming interface (API or web API) that conforms to the constraints
of REST architectural style and allows for interaction with RESTful web services. REST stands for representational state transfer.
For example, a REST API would use a GET request to retrieve a record, a POST request to create one, a PUT request to update a record,
and a DELETE request to delete one. All HTTP methods can be used in API calls. A well-designed REST API is similar to a website running
in a web browser with built-in HTTP functionality
