from flask import Flask, request, jsonify
from google.cloud import datastore

kind = "product-db"

datastore_client = datastore.Client()
app = Flask(__name__)

@app.route('/api/add', methods=['POST'])
def add_item():
    name = request.json['name']
    price = request.json['price']
    
    product_key = datastore_client.key(kind)
    product = datastore.Entity(key=product_key)
    product["name"] = name
    product["price"]= price

    datastore_client.put(product)
    return "" , 201

@app.route('/api/list', methods=['GET'])
def get_list():
    productlist = []
    productquery = datastore_client.query(kind=kind)
    products = productquery.fetch()
    
    for x in products: productlist.append(x)
    return jsonify(productlist)