from flask import Flask, render_template
import os

t_dir = os.path.abspath("app/templates")
s_dir = os.path.abspath("app/static")

app = Flask(__name__, template_folder=t_dir, static_folder=s_dir)

@app.route('/homepage')
def homepage():
  return render_template("homepage.html")
@app.route('/prices')
def prices():
    return render_template("prices.html")
@app.route('/contact')
def contact():
    return render_template('/contact.html')