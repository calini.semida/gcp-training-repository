In static routing routes, the path is updated by the user or an administrator, 
while in dynamic routing, routes are updated automatically.

The static routing is best for small network implementation whereas dynamic routing 
is best for a large network implementation. 

Static Routing requires less bandwidth than dynamic routing.

Static Routing does not use any routing protocols and algorithms, while dynamic routing 
uses routing protocols and complex algorithms to calculate routing operations.